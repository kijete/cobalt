-- Your SQL goes here
SET TIME ZONE 'UTC';
CREATE TABLE posts (
    id SERIAL PRIMARY KEY,
    is_thread BOOLEAN NOT NULL DEFAULT 't',
    parent_id INTEGER,
    subject VARCHAR(256),
    name VARCHAR(256),
    contact VARCHAR(256),
    body TEXT NOT NULL,
    image_link VARCHAR(256),
    password_hash VARCHAR(16),
    ip VARCHAR(64) NOT NULL,
    posted_time TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE bans (
    ip VARCHAR(64) NOT NULL PRIMARY KEY,
    reason TEXT,
    banned_time TIMESTAMP WITH TIME ZONE NOT NULL,
    unban_time TIMESTAMP WITH TIME ZONE
);