{
  description = "A simple imageboard made in rust.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    crane.url = "github:ipetkov/crane";
    crane.inputs.nixpkgs.follows = "nixpkgs";
    crane.inputs.flake-utils.follows = "flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    crane,
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = import nixpkgs {
          inherit system;
        };

        craneLib = crane.lib.${system};

        appName = "osmium";
        appVersion = "0.1.0";

        migrationFilter = path: _type: builtins.match "migrations/.*" path != null;
        sqlFilter = path: _type: builtins.match ".*sql$" path != null;
        projectFilter = path: type:
          (migrationFilter path type) || (craneLib.filterCargoSources path type) || (sqlFilter path type);

        src = pkgs.lib.cleanSourceWith {
          src = ./.;
          filter = projectFilter;
        };

        commonArgs = {
          inherit src;
          buildInputs = [pkgs.postgresql.lib];
        };

        cargoArtifacts = craneLib.buildDepsOnly commonArgs;

        rustBuild = craneLib.buildPackage (commonArgs
          // {
            inherit cargoArtifacts;

            pname = appName;
            version = appVersion;
          });

        dockerImage = pkgs.dockerTools.buildImage {
          name = appName + "-" + appVersion;
          config = {Entrypoint = ["${rustBuild}/bin/${appName}"];};

          copyToRoot = "./src/html_templates";
        };
      in {
        packages = {
          rustPackage = rustBuild;
          docker = dockerImage;
        };
        packages.default = dockerImage;
      }
    );
}
