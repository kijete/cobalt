NAME = osmium

build: build_release

build_release: prebuild
	cargo build --release
	@cp target/release/$(NAME) out/

build_debug: prebuild
	cargo build
	@cp target/debug/$(NAME) out/

prebuild:
	@rm -rf out
	@mkdir out
	@cp -r src/html_templates out/

clean:
	@rm -rf out/
	@cargo clean

run: run_release

run_%: build_%
	@cd out; ./$(NAME)