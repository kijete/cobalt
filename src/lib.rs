// This file is boilerplate for Rust's module system.
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate actix_web;
extern crate serde;

pub mod endpoints;
pub mod html_parser;
pub mod models;
pub mod reaper;
pub mod schema;
pub mod utils;
