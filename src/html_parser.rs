use crate::models::Post;
use crate::utils;
use once_cell::sync::Lazy;
use tera::{Context, Tera};

pub static TEMPLATES: Lazy<Tera> = Lazy::new(|| {
    let tera = match Tera::new("html_templates/**/*.html") {
        Ok(t) => t,
        Err(e) => {
            println!("Tera error: {}", e);
            ::std::process::exit(1);
        }
    };
    tera
});

pub fn error_page(text: String) -> String {
    return text; // TODO make a fancy and clean error page using the text error message
}

pub fn deleted_page(id: String) -> String {
    return id; // TODO make a fancy delete success page
}

pub fn redirect_post(thread_hex: String) -> String {
    return thread_hex; // TODO make a fancy redirect page
}

pub fn individual_post_html(post: Post) -> String {
    let mut context = Context::new();

    context.insert("id", &hex::encode(post.id.to_be_bytes()));
    context.insert("subject", &post.subject);
    context.insert("contact", &post.contact);
    context.insert("name", &post.name);
    context.insert(
        "password_hash",
        &match post.password_hash {
            Some(hash) => match utils::generate_bip_phrase(&hash) {
                Ok(bip) => bip,
                Err(_) => String::new(),
            },
            None => String::new(),
        },
    );
    context.insert("date", &post.posted_time.to_string());
    context.insert("body", &post.body);

    if post.is_thread {
        context.insert("thread_or_post", "thread")
    } else {
        context.insert("thread_or_post", "post")
    }

    return match TEMPLATES.render("post.html", &context) {
        Ok(s) => s,
        Err(e) => error_page(e.to_string()),
    };
}
