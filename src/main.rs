// This file is our main file. It starts the web server and the reaper, and contains the code to respond to requests.

#[macro_use]
extern crate diesel_migrations;

use actix_web::middleware::{Logger, TrailingSlash};
use actix_web::{middleware, web, App, HttpServer};
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;
use diesel_migrations::embed_migrations;
use log::{info, LevelFilter};
use osmium::endpoints::*;
use osmium::html_parser::TEMPLATES;
use osmium::reaper;
use simplelog::{ColorChoice, CombinedLogger, Config, TermLogger, TerminalMode};

// This embeds all of our migrations into the executable here,
// and creates a struct allowing us to run them on startup.
// Migrations setup our database tables into a usable state.
embed_migrations!();

// This allows us to use compiler flags to modify the logging level
// this runs in debug compile mode
#[cfg(debug_assertions)]
fn setup_logger() {
    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Warn,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        TermLogger::new(
            LevelFilter::Info,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        TermLogger::new(
            LevelFilter::Debug,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
    ])
    .unwrap();
}
// but this runs in release compile mode
#[cfg(not(debug_assertions))]
fn setup_logger() {
    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Warn,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        TermLogger::new(
            LevelFilter::Info,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        // No debug logger!
    ])
    .unwrap();
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Configure the logger
    setup_logger();
    info!("Logger started.");

    // TODO allow to accept multiple config files, so you can override a default
    // TODO replace every string with translatable.

    // Config needs to allow:
    // Setting the database url
    // Setting max posts before reaping
    // reap frequency?
    // bind address and port for nerds who dont proxy
    // whether to allow images or not -- will add much later, when all is complete

    // it also needs to be read from a folder ./osmium/ so docker mounts work
    // all files must go in ./osmium/file/<post_id>/<post_id>.extension
    // would be nice if they could be of arbitrary type, with configurable filtering
    // also, osmium should serve these files itself on /file/<post_id>
    // also, thumbnails
    // osmium will automatically downscale with imagemagick or something
    // thumbnails are stored at ./osmium/file/<post_id>/<post_id>_thumbnail.webp

    // on every run reaper will:
    // search all thread, and their replies.
    // find the newest of those replies for each thread
    // delete all but the n newest, by newest reply (configurable amount)
    // after this, reaper will then check all non-thread posts,
    // and delete all those for which the parent thread does not exist
    // then the reaper will remove all files stored on the server for which their post does not exist
    // and finally the reaper will check all expired bans and remove their entry

    info!("Initializing Tera templates...");
    for template in TEMPLATES.get_template_names() {
        // TEMPLATES is defined in the html_parser module, because it has to be in the library crate
        info!("Found template {}", template);
    }

    let database_url = "postgres://postgres:password@localhost/cobalt"; // TODO THIS IS ONLY FOR DEV REPLACE WITH REAL CONFIG

    info!("Initiating database connection...");
    // Our database manager for r2d2
    let manager = ConnectionManager::<PgConnection>::new(database_url);

    // r2d2 is a library allowing us to create a database connection pool.
    // I'm not sure why it is needed since diesel is synchronous.
    // Synchronous is fine for our low throughput, low volume, ephemeral data.
    let pool = Pool::builder().build(manager).expect(&format!(
        "Failed to create database pool at {}",
        database_url
    ));

    // The connection is returned to the pool when it goes out of scope after run() evaluates.
    embedded_migrations::run(&pool.get().expect("Couldn't get a connection on boot"))
        .expect("Unable to run initial migrations.");

    // Just run the reaper on every startup so we have a fresh clean database
    reaper::reap(pool.clone()).await;

    info!("Starting server...");
    // Now we make the server object and run it
    // TODO TLS
    HttpServer::new(move || {
        App::new()
            // Sets the scope of the endpoints. this allows for subdomains, such as for multiple boards
            .service(
                web::scope("/sus") // TODO allow for config
                    .app_data(web::Data::new(pool.clone())) // give the services database pool access
                    .service(main_page::main_page) // registers the services from their files
                    .service(make_post::make_post)
                    .service(get_thread::get_thread)
                    .service(delete_post::delete_post)
                    .service(get_post::get_post),
            )
            .wrap(Logger::default()) // enable logging
            .wrap(Logger::new("%a %{User-Agent}i"))
            // Remove duplicate and trailing slashes in the path so the resources can be accessed properly
            // i.e. /page/ turns into /page
            .wrap(middleware::NormalizePath::new(TrailingSlash::Trim))
    })
    .bind(("127.0.0.1", 8080))? // TODO allow for config
    .run()
    .await
}
