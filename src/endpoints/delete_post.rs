use crate::diesel::ExpressionMethods;
use crate::schema::posts;
use crate::{html_parser, utils};
use actix_web::http::header::Header;
use actix_web::{web, HttpRequest, HttpResponse};
use actix_web_httpauth::headers::authorization::{Authorization, Basic};
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::{PgConnection, QueryDsl, RunQueryDsl};
use log::info;

// Endpoint for deleting a thread with its password
// Remember -- the password is SHA3-256 hashed, and only the 64 highest bits are stored.
// This should be DELETE but HTML forms can't do that so POST works too
#[route("/delete/{post_id}", method = "DELETE", method = "POST")]
pub async fn delete_post(
    post_id: web::Path<String>,
    req: HttpRequest,
    pool: web::Data<Pool<ConnectionManager<PgConnection>>>,
) -> HttpResponse {
    // This converts the string in the path from hex to a numerical id.
    // It also errors kindly on incorrect hex.
    // also, this entire block here looks like Go. why can't we have an exhaustive try/catch?
    let delete_id = match utils::hex_decode_id(&post_id.into_inner()) {
        // Rust should implicitly change String to &'static str methinks
        Ok(did) => did,
        // Note: this is a function defined by cobalt to return a cute error page from a template using the given error text.
        Err(e) => {
            return HttpResponse::BadRequest().body(html_parser::error_page(e.to_string()));
        }
    };

    // Does the header have an HTTP Basic Authentication field?
    let auth = match Authorization::<Basic>::parse(&req) {
        Ok(auth) => auth,
        Err(e) => {
            return HttpResponse::BadRequest().body(html_parser::error_page(e.to_string()));
        }
    };

    // We don't use the username -- only the password field, as it is per post instead of per user
    let password = match auth.as_ref().password() {
        // is this safe?
        None => {
            return HttpResponse::BadRequest()
                .body(html_parser::error_page("No password provided!".to_string()));
        }
        Some(password) => password,
    };

    let hashed_password = utils::hash_password(password);

    // Now get a database connection to check if the post exists
    let conn = match pool.get() {
        Ok(conn) => conn,
        Err(_) => {
            return HttpResponse::InternalServerError().body(html_parser::error_page(
                "Internal database failure.\nPlease try again later.".to_string(),
            ));
        }
    };

    // dsl refers to our SQL row struct
    // This formats the SQL request to search for 1 post with the set id
    // and then sends it on our connection

    // Here's where we do the database access. It's in a blocking closure.
    let results = match web::block(move || {
        posts::table
            .select(posts::password_hash)
            .filter(posts::id.eq(delete_id))
            .limit(1)
            .load::<Option<String>>(&conn)
    })
    .await
    {
        // Return an error if we don't get it
        Ok(res) => {
            match res {
                // We have to match twice because it is a result of a result
                // the "await" result on the blocking
                // contains the "load" result on the db select
                Ok(r) => r,
                Err(e) => return unauth(e.to_string()),
            }
        }
        Err(_) => {
            return HttpResponse::InternalServerError().body(html_parser::error_page(
                "Internal processing failure.\nPlease try again later.".to_string(),
            ));
        }
    };

    // DON'T BLOCK DURING LOGIC

    // Check to see if the post even has a hashed password
    let post_hash = match &results[0] {
        None => return unauth("This post was made without a password".to_string()),
        Some(s) => s,
    };

    // Use index 0 since we set a limit of 1 post anyway
    info!("Attempt delete on: {}", post_hash);

    // If the password is right...
    // TODO allow delete of post by admin password(s)
    return if post_hash == &hashed_password {
        // get another database connection: the last one is invalid because we already used it in a closure
        // in earlier iterations this was all done in that blocking closure
        // but that blocks while we are doing logic which is not cool
        let conn = match pool.get() {
            Ok(conn) => conn,
            Err(_) => {
                return HttpResponse::InternalServerError().body(html_parser::error_page(
                    "Internal database failure.\nPlease try again later.".to_string(),
                ));
            }
        };

        // Delete all posts with that id. which should be only one, but if there's multiple they should be deleted anyway
        return match web::block(move || {
            diesel::delete(posts::table.filter(posts::id.eq(delete_id))).execute(&conn)
        })
        .await
        {
            // in our new and improved not-blocking-everything version, our second conn goes
            // out of scope here

            // this is where we should terminate if all goes well
            Ok(_) => HttpResponse::Ok().body(html_parser::deleted_page(format!(
                "Successfully deleted post with id {}",
                delete_id.to_string()
            ))),
            // whoops! database error
            Err(_) => HttpResponse::InternalServerError()
                .body("Deletion error.\nPlease try again later".to_string()),
        };
    } else {
        // Otherwise tell them no
        unauth("Wrong password.".to_string())
    };
}

// so we dont have length http responses everywhere
fn unauth(e: String) -> HttpResponse {
    HttpResponse::Unauthorized().body(html_parser::error_page(e.to_string()))
}
