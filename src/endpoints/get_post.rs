use crate::diesel::ExpressionMethods;
use crate::models::Post;
use crate::schema::posts;
use crate::{html_parser, utils};
use actix_web::{web, HttpResponse};
use diesel::r2d2::ConnectionManager;
use diesel::{PgConnection, QueryDsl, RunQueryDsl};
use diesel::r2d2::Pool;

// Endpoint for accessing a specific POST by its 32 bit hex id
// will only show a single post, it is usually only used to generate previews for hovering over link
#[get("/post/{string_id}")]
pub async fn get_post(
    string_id: web::Path<String>,
    pool: web::Data<Pool<ConnectionManager<PgConnection>>>,
) -> HttpResponse {
    // TODO make this function too
    return match fetch_post_data(string_id, pool).await {
        Ok(post) => HttpResponse::Ok().body(html_parser::individual_post_html(post)),
        Err(error) => HttpResponse::InternalServerError().body(html_parser::error_page(error)),
    };
}

pub async fn fetch_post_data(
    string_id: web::Path<String>,
    pool: web::Data<Pool<ConnectionManager<PgConnection>>>,
) -> Result<Post, String> {
    // So the code is more readable
    let server_error =
        html_parser::error_page("Internal database failure.\nPlease try again later.".to_string());

    // This converts the string in the path from hex to a numerical id.
    // It also errors kindly on incorrect hex.
    let id = match utils::hex_decode_id(&string_id.into_inner()) {
        // Rust should implicitly change String to &'static str methinks
        Ok(id) => id,
        Err(e) => {
            return Err(html_parser::error_page(e.to_string()));
        }
    };

    // Now get a database connection to check if the post exists
    let conn = match pool.get() {
        Ok(conn) => conn,
        Err(_) => return Err(server_error),
    };

    let post_vec = match web::block(move || {
        posts::table
            .select(posts::all_columns)
            .filter(posts::id.eq(id))
            .limit(1)
            .load::<Post>(&conn)
    })
    .await
    {
        Ok(p) => match p {
            Ok(post_vec) => post_vec,
            Err(_) => return Err(server_error),
        },
        Err(_) => return Err(server_error),
    };

    // Because diesel spits out like five fucking nested results and options.
    let post = match post_vec.into_iter().next() {
        // Turning it into an iter means it gives up ownership of the post.
        None => return Err(server_error),
        Some(p) => p,
    };

    return Ok(post);
}
