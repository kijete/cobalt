use actix_web::HttpResponse;

// Endpoint for the front page
#[get("")]
pub async fn main_page() -> HttpResponse {
    // TODO make frontpage
    HttpResponse::Ok().body("hello")
}
