use crate::{html_parser, utils};
use actix_web::{web, HttpResponse};

// Endpoint for accessing a specific thread by its 32 bit hex id
#[get("/thread/{string_id}")]
pub async fn get_thread(string_id: web::Path<String>) -> HttpResponse {
    // TODO make this function
    // This converts the string in the path from hex to a numerical id.
    // It also errors kindly on incorrect hex.
    let id = match utils::hex_decode_id(&string_id.into_inner()) {
        // Rust should implicitly change String to &'static str methinks
        Ok(id) => id,
        Err(e) => {
            return HttpResponse::BadRequest().body(html_parser::error_page(e.to_string()));
        }
    };

    return HttpResponse::Ok().body(format!("{:x}", id));
}
