use crate::models::{InsertablePost, Post};
use crate::schema::posts;
use crate::{html_parser, utils};
use actix_multipart::Multipart;
use actix_web::{web, HttpRequest, HttpResponse};
use chrono::Utc;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::{insert_into, PgConnection, RunQueryDsl};
use futures_util::stream::StreamExt;
use log::debug;


// Endpoint for making a post
// Takes multipart/form-data for http POST
// apparently POST can use multiple formats
#[post("/post")]
pub async fn make_post(
    mut payload: Multipart,
    req: HttpRequest,
    pool: web::Data<Pool<ConnectionManager<PgConnection>>>,
) -> HttpResponse {
    // Make a struct to save all of our fields to, then insert into the DB
    let mut new_post = InsertablePost {
        // I know it doesn't fit the style guide but it's a struct so screw you
        is_thread: true,
        parent_id: None, // in form but we need to decode the hex

        subject: None,        // in form
        name: None,           // in form
        contact: None,        // in form
        body: "".to_string(), // in form

        image_link: None,    // in form but as an image we have to handle uploading
        password_hash: None, // in form as password we need to hash
        ip: "".to_string(),
        posted_time: Utc::now().naive_utc(), // easily sets the timestamp to a time of now, as soon as post is received
    };

    // Get their ip even behind a remote proxy. Not secure but good enough
    new_post.ip = match req.connection_info().realip_remote_addr() {
        None => {
            return HttpResponse::BadRequest().body(html_parser::error_page(
                "IP could not be determined".to_string(),
            ))
        }
        Some(ip) => ip.to_string(),
    };

    // Iterate over all the streams in the Multipart
    while let Some(item) = payload.next().await {
        // Make sure we have fields in the payload
        let mut field = match item {
            Ok(f) => f,
            Err(e) => {
                return HttpResponse::BadRequest().body(html_parser::error_page(e.to_string()))
            }
        };

        // TODO check ip for ban list

        // TODO file processing here

        // Here is the field processing for non files
        // I'm not sure why we have to process chunks from each field but. oh well
        while let Some(some_chunk) = field.next().await {
            // Get the chunks of data out of the field
            let chunk = match some_chunk {
                Ok(c) => c,
                Err(e) => {
                    return HttpResponse::BadRequest().body(html_parser::error_page(e.to_string()))
                } // Split payload error most likely
            };

            let string_data = match std::str::from_utf8(&chunk) {
                Ok(s) => s,
                Err(e) => {
                    return HttpResponse::BadRequest().body(html_parser::error_page(e.to_string()))
                } // A UTF-8 error. We can't handle anything else so yell at the user
            };

            debug!(
                "Got this form data: {}={}",
                field.content_disposition().get_name().unwrap_or(""),
                string_data
            );

            // Get the name of each content field in the multipart field
            // And match the name to each field in our struct
            // Giving it the string we just got
            // See the RFC for http multipart if you're confused
            match field.content_disposition().get_name() {
                // If it has no name it's the wrong type so we don't need it
                None => {} // since this is the end of the loop, {} == {continue;}

                // if it has a name (which it should) find out what type of field it is.
                Some(name) => {
                    // nasty code which matches each field to its storage
                    // LOOK HERE to find out the name for each field when setting up the form
                    match name {
                        "parent_id" => {
                            // if it has a parent it's not a thread
                            new_post.is_thread = false;
                            // technically this lets us make semi-orphans:
                            // Posts whose thread-parents are children themselves
                            // These will never get rendered because their thread is never rendered
                            // ( it doesnt exist )
                            // and will get reaped on the next cycle (reaper checks for thread)
                            // so it does not matter
                            new_post.parent_id =
                                Some(match utils::hex_decode_id(&string_data.to_string()) {
                                    Ok(i) => i,
                                    // if there's an invalid parent id we are completely borked.
                                    // we can't create a new thread, because they clearly meant to reply
                                    // we can't reply to a non-existing thread, nobody could read it
                                    // so we just give up and die
                                    Err(e) => {
                                        return HttpResponse::BadRequest()
                                            .body(html_parser::error_page(e.to_string()));
                                    }
                                });
                        }

                        "subject" => new_post.subject = Some(string_data.to_string()),
                        "name" => new_post.name = Some(string_data.to_string()),
                        "contact" => {
                            // TODO sanity check
                            new_post.contact = Some(string_data.to_string())
                        }

                        // This one is not an option
                        "body" => new_post.body = string_data.to_string(),

                        // remember to hash the password BEFORE saving it
                        "password" => {
                            new_post.password_hash = Some(utils::hash_password(string_data))
                        }

                        // the only things not handled in this match block are:
                        // image_link,
                        // id,
                        // ip,
                        // we skip the image/file link, as that is handled upon uploading (see above)
                        // postgres does the id for us
                        // the ip is handled way back up at the beginning
                        &_ => {} // anything else, just skip it lol
                    }
                }
            }
        }
    }
    // right after that unholy nest of code, we should have a properly formatted post struct
    debug!("Properly formatted post struct: {:?}", new_post);

    // Now get a database connection to add the new post
    let conn = match pool.get() {
        Ok(conn) => conn,
        Err(_) => {
            return HttpResponse::InternalServerError().body(html_parser::error_page(
                "Internal database failure.\nPlease try again later.".to_string(),
            ));
        }
    };
    // Now try to insert it and return a result!
    return match web::block(move || {
        insert_into(posts::table)
            .values(&new_post)
            .get_result::<Post>(&conn)
    })
    .await
    {
        // We only held the database connection for one line there!
        // then the blocking ended because we are done with it
        Ok(inserted_post) => {
            // Get the id of the thread the post is in
            // It could be the id of the post,
            // or the parent_id if it has one
            let thread_id = match inserted_post {
                Ok(post) => match post.parent_id {
                    None => post.id,
                    Some(p) => p,
                },
                Err(_) => {
                    return HttpResponse::InternalServerError().body(html_parser::error_page(
                        "Internal database failure.\nThe post was likely made, but we cannot redirect you.".to_string()));
                }
            };

            HttpResponse::Ok().body(html_parser::redirect_post(hex::encode(
                thread_id.to_be_bytes(),
            )))
        }
        // The user doesn't need to know WHY it failed inserting, just that it did
        // That would represent a vulnerability in already buggy code
        // (neither do i know)
        Err(_) => HttpResponse::InternalServerError().body(html_parser::error_page(
            "Internal database failure.\nPlease try again later.".to_string(),
        )),
    };
}
