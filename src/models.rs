// This file contains the structs used by Diesel to manage our database.

use crate::schema::posts;
use chrono::NaiveDateTime;
use diesel::Queryable;

// time timestamp types are all timezone-naive so we must always use utc

#[derive(Queryable, Debug)]
pub struct Post {
    pub id: i32,
    pub is_thread: bool,
    pub parent_id: Option<i32>,

    pub subject: Option<String>,
    pub name: Option<String>,
    pub contact: Option<String>, // Preferred to be a mailto:// or xmpp://

    pub body: String,
    pub image_link: Option<String>, // Should be absolute, for ease of use, and also external links

    pub password_hash: Option<String>, // Most significant 64 bits of the SHA3-256 hash of the password in hex. Not secure, but good enough
    // Publicly displayed for user verification as a BIP 39 code, 6 words long
    pub ip: String, // just store it as a fucking string man

    pub posted_time: NaiveDateTime, // forgot the fukin timestamp im stupid
}

// we use this to insert data into the database
// it is different because some fields (the id) are handled by postgres
// we should not set those ourself
#[derive(Insertable, Debug)]
#[table_name = "posts"]
pub struct InsertablePost {
    // this one has no id since we are not allowed to insert the id ourself
    pub is_thread: bool,
    pub parent_id: Option<i32>,

    pub subject: Option<String>,
    pub name: Option<String>,
    pub contact: Option<String>, // Preferred to be a mailto:// or xmpp://

    pub body: String,
    pub image_link: Option<String>, // Should be absolute, for ease of use, and also external links

    pub password_hash: Option<String>, // Most significant 64 bits of the SHA3-256 hash of the password in hex. Not secure, but good enough
    // Publicly displayed for user verification as a BIP 39 code, 6 words long
    pub ip: String, // just store it as a fucking string man
    pub posted_time: NaiveDateTime,
}

// a struct storing info about banned users
// mostly self explanatory
// reason and unban are optional lol
#[derive(Queryable, Debug)]
pub struct BannedUser {
    pub ip: String,
    pub reason: Option<String>,
    pub banned_time: NaiveDateTime,
    pub unban_time: Option<NaiveDateTime>,
}
