// This file has simple utils such as hashing a password and turning said hash into a BIP39 string

mod bip39_english;

use log::debug;
use sha3::{Digest, Sha3_256};
use std::num::ParseIntError;

// This is just so i can change the implementation if str_radix fucks up
// also it looks cleaner
pub fn hex_decode_id(input: &String) -> Result<i32, ParseIntError> {
    i32::from_str_radix(input.as_str(), 16)
}
pub fn hex_decode_hash(input: &String) -> Result<u64, ParseIntError> {
    u64::from_str_radix(input.as_str(), 16)
}

// Turn the password into a unique String. Not secure, it just chops of the highest 64 bits of SHA3_256
pub fn hash_password(password: &str) -> String {
    // Let's also get the hash from the password.
    let mut hasher = Sha3_256::new();
    hasher.update(password.as_bytes()); // It is in UTF-8 i think

    // These lines are separate so this result can live longer than one line
    // This is important to the compiler for some reason
    let long_lived = hasher.finalize();
    let full_hash = long_lived.as_slice().clone();

    // Then get the first 8 bytes and package them into hex
    let mut buf = [0u8; 8];
    buf.copy_from_slice(&full_hash[0..8]);
    // Is it big endian or little endian? No fucking idea, because the devs are allergic to documentation.
    // Ultimately it doesn't matter -- it is unique and consistent.
    // I tested and this is correct
    return hex::encode(buf);
}

pub fn generate_bip_phrase(password_hash: &String) -> Result<String, ParseIntError> {
    let hash_number = match hex_decode_hash(password_hash) {
        Ok(num) => num,
        Err(e) => return Err(e),
    };
    debug!(
        "Binary representation of hash to be turned into BIP39: {:b}",
        hash_number
    );

    // Create an array of 11 bit numbers (the top 5 will be 0)
    let mut word_numbers_array = [0usize; 6];

    // Iterate once for each in the array.
    let mask: u64 = 2047 << (64 - 11); // A mask we can shift and & with the number
    for i in 0..5 {
        let unshifted = hash_number & (mask >> i * 11); // mask out the 11 bit segment we want

        // This one is a little confusing
        // (5-i)*11 is the amount of space between the last bit in our previously masked segment
        // and the end of the number. however, we have a 64 bit number, not 66,
        // so we need to account for that and subtract 2 from the amount shifted
        // this has the added benefit that the final segment will be shifted LEFT twice,
        // adding two zeros to the end to make it the proper size (8 to 11)
        // we & with 2048 to make sure it is only 11 bit, then cast to u16
        // this is very unsafe code. but we don't need an unsafe block for some reason
        let shifted = (unshifted >> ((5 - i) * 11) - 2) & 2047;
        debug!("BIP39 word number: {:b} or {}", shifted, shifted);
        word_numbers_array[i] = shifted as usize;
    }

    // Now we just lookup each word and concatenate it to our buffer
    let mut stringbuf = String::from("");
    for word_number in word_numbers_array {
        stringbuf = stringbuf + ":" + bip39_english::WORDS[word_number];
    }
    return Ok(stringbuf);
}
