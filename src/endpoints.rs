// This is simple boilerplate exposing the endpoints as public functions
// The files in endpoints/ are obviously all endpoints

pub mod delete_post;
pub mod get_post;
pub mod get_thread;
pub mod main_page;
pub mod make_post;
