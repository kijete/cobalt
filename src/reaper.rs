// Do do do do
// do do do do
// do do do do
// do do do do
// don't fear him

// As explained in main.rs
// on every run reaper will:

// search all thread, and their replies.
// find the newest of those replies for each thread
// delete all but the n newest, by newest reply (configurable amount)

// after this, reaper will then check all non-thread posts,
// and delete all those for which the parent thread does not exist

// then the reaper will remove all files stored on the server for which their post does not exist
// TODO add file reaping

// and finally the reaper will check all expired bans and remove their entry

use crate::diesel::ExpressionMethods;
use crate::schema::posts;
use actix_web::web;
use chrono::NaiveDateTime;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::{PgConnection, QueryDsl, RunQueryDsl};
use log::{debug, error, info};
use std::collections::{BTreeMap, HashSet};
use std::error::Error;

// do all of the reaping
// also do all of the error managment and retries here
// for consistency
pub async fn reap(pool: Pool<ConnectionManager<PgConnection>>) {
    info!("Beginning reaper");
    for i in 1..15 {
        match reap_expired(pool.clone()).await {
            Ok(_) => {
                break;
            } // Leave the loop
            Err(e) => {
                error!(
                    "The thread reaper errored with on try {i}: {}",
                    e.to_string()
                )
            }
        }
    }
    info!("Now reaping orphans...");
    for i in 1..15 {
        match reap_orphans(pool.clone()).await {
            Ok(_) => {
                break;
            } // Leave the loop
            Err(e) => {
                error!(
                    "The orphan reaper errored with on try {i}: {}",
                    e.to_string()
                )
            }
        }
    }
}

// Reaps all but the n newest threads
// newest is defined by newest comment
async fn reap_expired(pool: Pool<ConnectionManager<PgConnection>>) -> Result<(), Box<dyn Error>> {
    // Get every thread
    // but only the information we need for memory reasons
    // we get the posted time and the id
    // we need the id to find the children
    // remember -- UTC
    let conn = pool.get()?;

    let thread = web::block(move || {
        posts::table
            .select((posts::id, posts::posted_time))
            .filter(posts::is_thread.eq(true))
            .load::<(i32, NaiveDateTime)>(&conn)
    })
    .await??; // scuffed but im glad we can propagate errors here

    // conn IS NOW OUT OF SCOPE GET A NEW ONE NEXT TIME

    // A B-tree is the ideal map to sort with
    // i think. i'm not sure
    // we sort it by date to get the id of each thread in order of age
    let mut thread_map = BTreeMap::<NaiveDateTime, i32>::new();

    for th in thread {
        let thread_id = th.0;
        let mut latest_time = th.1;

        debug!("Checking thread {} for reaping", thread_id);

        // we need another conn for every thread
        // this is because we need to get their children
        let conn = pool.get()?;
        let newest_child_option_vec = web::block(move || {
            posts::table
                .select(posts::posted_time)
                .filter(posts::parent_id.eq(thread_id))
                // We can sort the children by date
                .order(posts::posted_time.desc())
                // and pick only the first (newest) one...
                .limit(1)
                .load::<NaiveDateTime>(&conn) // turbofish activated
        })
        .await??;

        // Since we set limit(1) it is okay to use first
        // it will return None if there are no children however.
        match newest_child_option_vec.first() {
            None => {}
            Some(c) => {
                // The sql query has done the work for us
                // all we need to do is check the age and see if the child is older
                // it should always be but you gotta make sure
                // seeing as you can make posts on threads that don't exist
                if c > &latest_time {
                    latest_time = c.clone()
                }
            }
        }
        // now just put the data in the map!
        thread_map.insert(latest_time, thread_id);

        // a monstrosity which only should run in debug
        // if a thread has no children, the child is from Unix day
        debug!(
            "Newest child on thread {} is age {}",
            thread_id,
            newest_child_option_vec
                .first()
                .unwrap_or(&NaiveDateTime::default())
                .to_string()
        );
    }
    let saved_n_threads = 100; // TODO make a config for this

    // We can collect a BTreeMap into a sorted vec of the ids
    // as it is in ascending order, that means it goes smallest to largest
    // or oldest to youngest
    let sorted_thread_ids: Vec<i32> = thread_map.values().cloned().collect();
    // cloned clones every value in the Map so we can clone the map easily
    // we need to clone the map to move it into a closure without lifetime errors

    // so we need to save the LAST n threads
    // which means delete the first Total-n threads to leave n remaining
    // BUT FIRST CHECK TO MAKE SURE WE DONT SUBTRACT TO MUCH FROM A usize
    // if we have less threads than n, then we can't reap any
    // in that case we just go home we're done
    if saved_n_threads >= sorted_thread_ids.len() {
        info!("Not enough threads to reap!");
        return Ok(());
    }
    let amount_to_reap = sorted_thread_ids.len() - saved_n_threads;

    let mut reaping_threads = Vec::new();
    reaping_threads.copy_from_slice(&sorted_thread_ids[..amount_to_reap]);

    let conn = pool.get()?;
    web::block(move || {
        diesel::delete(posts::table)
            .filter(posts::id.eq_any(reaping_threads))
            .execute(&conn)
    })
    .await??;

    info!(
        "{} dead threads have been reaped successfully",
        &sorted_thread_ids[..amount_to_reap].len()
    );

    return Ok(());
}

// We call this after the above function
// To delete all the posts who's thread parent does not exist anymore
async fn reap_orphans(pool: Pool<ConnectionManager<PgConnection>>) -> Result<(), Box<dyn Error>> {
    // Get every thread
    // but only the information we need for memory reasons
    // we only need to get the id and parent id
    let conn = pool.get()?;

    let posts = web::block(move || {
        posts::table
            .select((posts::id, posts::parent_id))
            .load::<(i32, Option<i32>)>(&conn)
    })
    .await??; // scuffed but im glad we can propagate errors here

    let mut living_thread_ids = HashSet::<i32>::new();
    let mut living_children = HashSet::<(i32, i32)>::new(); // this maps thread_id to children id in that order

    for p in posts {
        // p.1 is the parent_id OPTION
        match p.1 {
            // If there is no parent_id, then this represents a THREAD which EXISTS
            None => {
                living_thread_ids.insert(p.0);
            } // insert the corresponding id
            // otherwise, it is a CHILD with a PARENT
            Some(thread_id) => {
                living_children.insert((thread_id, p.0));
            }
        }
    }

    // A set of all the children ids we need to delete
    let mut reaping_posts = HashSet::<i32>::new();

    // somewhat slow: it iterates over every single non-thread post in the database
    // we can just throw it on a new cpu thread though: it isn't intense
    // it doesn't block our synchronous db operations
    for (thread_id, child_id) in living_children {
        // If the thread id of the child is not extant...
        if !living_thread_ids.contains(&thread_id) {
            // ... add the child to be deleted
            reaping_posts.insert(child_id);
        }
    }

    // Now just delete all the dead children... lol
    let conn = pool.get()?;
    web::block(move || {
        diesel::delete(posts::table)
            .filter(posts::id.eq_any(reaping_posts))
            .execute(&conn)
    })
    .await??;

    return Ok(());
}
