table! {
    bans (ip) {
        ip -> Varchar,
        reason -> Nullable<Text>,
        banned_time -> Timestamptz,
        unban_time -> Nullable<Timestamptz>,
    }
}

table! {
    posts (id) {
        id -> Int4,
        is_thread -> Bool,
        parent_id -> Nullable<Int4>,
        subject -> Nullable<Varchar>,
        name -> Nullable<Varchar>,
        contact -> Nullable<Varchar>,
        body -> Text,
        image_link -> Nullable<Varchar>,
        password_hash -> Nullable<Varchar>,
        ip -> Varchar,
        posted_time -> Timestamptz,
    }
}

allow_tables_to_appear_in_same_query!(bans, posts,);
